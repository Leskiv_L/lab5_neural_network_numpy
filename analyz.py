import neural_network_v2 as nn
import numpy as np

t_x = np.array([[0, 0, 1, 1],
                    [0, 1, 0, 1]])

t_y = np.array([[0, 0, 0, 1]])

epoch1 = 100
epoch2 = 500
epoch3 = 1000
epoch4 = 4000

n = nn.NeuralNetwork()

print('\t\tОчікувані результати\n [[0, 0, 0, 1]]')

n.train(t_x, t_y, epoch1)
print('\t\tРезультат для epoch = 100\n', n.think(t_x))

n.train(t_x, t_y, epoch2)
print('\t\tРезультат для epoch = 500\n', n.think(t_x))

n.train(t_x, t_y, epoch3)
print('\t\tРезультат для epoch = 1000\n', n.think(t_x))

n.train(t_x, t_y, epoch4)
print('\t\tРезультат для epoch = 4000\n', n.think(t_x))