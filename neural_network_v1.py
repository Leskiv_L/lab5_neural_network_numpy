import numpy as np
import math


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def neural_network(x, y, alpha=0.1, eps=0.0001, epoch=1000):
    # create initial weights
    np.random.seed(1)
    weights = np.zeros((2, 1))
    print("Рандомні ваги:\n", weights)

    m = len(x)
    b = 0
    j = [0]
    dw = 0
    db = 0
    result = []
    for item in range(epoch):
        # forward
        z = np.dot(weights.T, x) + b
        a = sigmoid(z)
        j.append(-(y * np.log(a) + (1 - y) * np.log(1 - a)))

        # backward
        dz = a - y
        dw = (1 / m) * np.dot(x, dz.T)
        db = (1 / m) * np.sum(dz)

        weights = weights - alpha * dw
        b = b - alpha * db

        dj = j[item] - j[item - 1]
        # condition
        if np.all(abs(dj) <= eps):
            break

    for i in a:
        for j in a[0]:
            if np.all(j >= 0.5):
                result.append(1)
            else:
                result.append(0)
    print("Ваги після навчання: \n", weights)
    return a, result, dj

def plot_cost_function():
    for i in dj:
        pyplot.plot(i, 'ro--')
        pyplot.xlabel("item")
        pyplot.ylabel("J")
        pyplot.title("COST FUNCTION")
        pyplot.legend(['J = -(1/m) * (y * np.log(a) + (1 - y) * np.log(1 - a))'])
        pyplot.grid(True)
        pyplot.show()

def plot_classifier():
    t = np.arange(-1,2,0.2)
    y = np.tan(40)*t+1.5
    pyplot.plot(t,y,
                x[0],x[1],'go'
    pyplot.plot(t,y, 'ro--',
         )
    pyplot.xlabel('x')
    pyplot.ylabel('y')
    pyplot.title('CLASSIFIER')
    pyplot.legend(['classifier',
           'point of array x'])
    pyplot.grid(True)
    pyplot.show()
    
    
if __name__ == "__main__":
    x = np.array([[0, 0],
                  [0, 1],
                  [1, 0],
                  [1, 1]]).T

    y = np.array([[0, 0, 0, 1]])
    a, result, dj = neural_network(x, y)
    print("Результат навчання: \n", a)
    print("Округлені результати:\n", result)
    print('Функція втрат:\n', dj)
    plot_cost_function()
    plot_classifier()
